## Repo information

This repo contains the assignment for Benjamin Muller's Machine Learning for Natural Language Processing course (ENSAE Paris, 2022, Réda Tawfiki, Michel Capot).
As requested, it consists of a runnable colab **notebook** + a 2-page-pdf **report**.